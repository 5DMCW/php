CREATE TABLE IF NOT EXISTS `articles` (
  `IdArticle` int(10) NOT NULL AUTO_INCREMENT,
  `TitleArticle` varchar(255) NOT NULL,
  `ContentArticle` text NOT NULL,
  `DateArticle` datetime NOT NULL,
  `AuthorArticle` varchar(100) NOT NULL,
  PRIMARY KEY (`IdArticle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;



CREATE TABLE IF NOT EXISTS `comments` (
  `IdComment` int(10) NOT NULL AUTO_INCREMENT,
  `PseudoComment` varchar(20) NOT NULL,
  `TextComment` text NOT NULL,
  `DateComment` datetime NOT NULL,
  `IdArticle` int(10) NOT NULL,
  PRIMARY KEY (`IdComment`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
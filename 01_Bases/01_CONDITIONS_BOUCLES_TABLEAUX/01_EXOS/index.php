<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>PHP - Bases</title>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="page">
		<div id="header">
			<h1>bases du PHP</h1>
		</div>	
		<div id="col_left">
		<form>
			<input type="text" placeholder="Recherche d'un jour" />
			<input type="submit" value="OK" />
		</form>
		<ul class="menu">
			<li><a class="actif" href="index.php">Foreach</a></li>
			<li><a class="" href="index.php">For</a></li>
			<li><a class="" href="index.php">While</a></li>
		</ul>
		</div>
		
		<div id="col_right">

			<h2>La boucle foreach</h2>
			<h3>Table HTML</h3>
			<table cellpadding="0" cellspacing="0" class="calendrier" width="100%">
					<tr>
					<th>
						<a class="" href="index.php">
							Jour
						</a>
					</th>
					</tr>
			</table>

			<h2>La boucle for</h2>
			<h3>Liste HTML</h3>
			<ul class="calendrier">
					<li>
						<a class="" href="index.php">
							Jour
						</a>
					</li>
			</ul>
			
			<h2>La boucle while</h2>
			<h3>Paragraphe HTML</h3>
			<p class="calendrier">
				<a class="" href="index.php">
					Jour
				</a>
			</p>
		
	</div>
	
	<div id="bas_page">	
	</div>
</div>
<div id="footer">
	&copy; 2011 - Bases du PHP
</div>
</body>
</html>
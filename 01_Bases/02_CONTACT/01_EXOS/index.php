<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>PHP - Contact</title>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="page">
		<div id="header">
			<h1>Le formulaire de contact</h1>
		</div>	
		<div id="col_left">
		</div>
		
		<div id="col_right">
			<h2>Contact</h2>
			<form action="index.php" method="post">
				<p>
				<input type="radio" name="genre" value="Mr." id="mr" /> <label for="mr">Monsieur</label>
				<input type="radio" name="genre" value="Mme" id="mme" /> <label for="mme">Madame</label>
				</p>
				<p>
				<label for="nom">Nom*</label>
				<span class="error" title="Vous pouvez indiquer votre prénom et nom dans le même champ.">Veuillez indiquer votre nom</span>
				<br />
				<input type="text" name="nom" id="nom" />
				</p>
				<p>
				<label for="email">E-mail*</label>
				<span class="error" title="Le message vous sera également transmis">Veuillez indiquer votre adresse e-mail</span>
				<span class="error" title="Au format [nom@nomdedomaine.ext]">Veuillez indiquer une adresse valide</span>
				<br />
				<input type="text" name="email" id="email" />
				</p>
				<p>
				<label for="sujet">Sujet*</label>
				<span class="error" title="Aidez-nous à comprendre la nature de votre requête">Veuillez définir un sujet</span>
				<br />
				<select name="sujet" id="sujet">
					<option value=""></option>
					<option value="renseignements">Demande de renseignements</option>
					<option value="autre">Autre</option>
				</select>
				</p>
				<p>
				<label for="message">Message*</label>
				<span class="error" title="Sans doute un oubli">Veuillez indiquer un message</span>
				<br />
				<textarea name="message" id="message"></textarea>
				</p>
				<p>
				<input type="checkbox" name="newsletter" id="newsletter" />
				<label for="newsletter">Je souhaite recevoir la newsletter</label>
				</p>
				<p>
				<input type="submit" value="Envoyer le message" />
				</p>
				
			</form>			
		</div>
	
		<div id="bas_page">	
		</div>
	</div>
<div id="footer">
	&copy; 2011 - Galerie (<a href="index_bd.php">Site avec BD</a>)
</div>
</body>
</html>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>PHP - Site multilingue</title>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="page">
		<div id="header">
			<h1>Site multilingue</h1>
		</div>	
		<div id="col_left">
    		<ul class="menu">
				<li><a class="" href="index.php">Accueil</a></li>
				<li><a class="" href="contact.php">Contact</a></li>
			</ul>
		</div>
		
		<div id="col_right">
    		<div id="langue">
				<a href="index.php">fr</a> | <a href="index.php">en</a>
			</div>
			<p>
	    		<label for="nom">Nom<label><br />
	    	    <input id="nom" type="text" /><br />
			</p>
			<p>
	    		<label for="message">Message<label><br/ >
	    	    <textarea id="message"></textarea>
			</p>
			<p>
				<input type="submit" value="Envoyer le message">
			</p>
		</div>
	
		<div id="bas_page">	
		</div>
</div>
<div id="footer">
	&copy; 2011 - Multilingue
</div>
</body>
</html>
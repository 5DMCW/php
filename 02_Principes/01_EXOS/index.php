<!DOCTYPE html>
<html>
<head>
    <title>PHP</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif|Roboto" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div class="container">
        
        <header>
            <nav>
                
                <?php include 'login.php'; ?>
                
                <a href=""><img src="images/logo.svg"> Site Web</a>
                
                <?php include 'menu.php'; ?>
                
            </nav>
        </header>
        
        <main>
            
            <?php include 'articles.php' ?>
            
        </main>
    </div>
</body>
</html>

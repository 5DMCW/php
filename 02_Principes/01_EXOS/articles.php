<article>
    <a href="">
        <h2>Analyse de projet et recherche</h2>
        <p>Identifier le point de convergence entre les intentions (vendre, fidéliser, informer,...), celles des utilisateurs et les possibilités technologiques.</p>
    </a>
    <img src="images/mobile-structure.svg">
</article>
<article>
    <a href="">
        <h2>Support & SEO - Stratégie de communication</h2>
        <p>Définir la stratégie dans laquelle l'outil s'inscrit sur le plan de la communication et du marketing.  Situer la forme du message (langage) à utiliser.</p>
    </a>
    <img src="images/communication.svg">
</article>
<article>
    <a href="">
        <h2>Structure - Architeture du contenu</h2>
        <p>Définir la stratégie dans laquelle l'outil s'inscrit sur le plan de la communication et du marketing.  Situer la forme du message (langage) à utiliser.</p>
    </a>
    <img src="images/wireframe.svg">
</article>
<article>
    <a href="">
        <h2>Design & Utilisation - Prototype</h2>
        <p>Mise en contexte d’utilisation des interfaces et des fonctionnalités clés. Permet de constater l’agissement du contenu sur les différents supports.</p>
    </a>
    <img src="images/prototype.svg">
</article>
        
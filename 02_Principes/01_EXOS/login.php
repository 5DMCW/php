<?php 
session_start();
?>

<?php 
if( isset( $_SESSION['login'] ) )
{
    ?>
    <form action="index.php?logout" method="post">
        <button>Déconnexion</button>
    </form>
    <?php
}
else
{
    ?>
    <form action="index.php" method="post">
        <input type="email" name="email">
        <input type="password" name="password">
        <button>OK</button>
    </form>
    <?php
}
-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 10 Mai 2018 à 13:56
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `php`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `IdArticle` int(11) NOT NULL AUTO_INCREMENT,
  `TitleArticle` varchar(255) NOT NULL,
  `ContentArticle` text NOT NULL,
  `ImageArticle` varchar(100) NOT NULL,
  `UrlArticle` varchar(100) NOT NULL,
  PRIMARY KEY (`IdArticle`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`IdArticle`, `TitleArticle`, `ContentArticle`, `ImageArticle`, `UrlArticle`) VALUES
(1, 'Analyse de projet et recherche', 'Identifier le point de convergence entre les intentions (vendre, fid&eacute;liser, informer,...), celles des utilisateurs et les possibilit&eacute;s technologiques.', 'mobile-structure.svg', 'index.php'),
(2, 'Support & SEO - Strat&eacute;gie de communication', 'D&eacute;finir la strat&eacute;gie dans laquelle l''outil s''inscrit sur le plan de la communication et du marketing. Situer la forme du message (langage) &agrave; utiliser.', 'communication.svg', 'index.php'),
(3, 'Structure - Architeture du contenu', 'D&eacute;finir la strat&eacute;gie dans laquelle l''outil s''inscrit sur le plan de la communication et du marketing. Situer la forme du message (langage) &agrave; utiliser.', 'wireframe.svg', ''),
(4, 'Design & Utilisation - Prototype', 'Mise en contexte d''utilisation des interfaces et des fonctionnalit&eacute;s cl&eacute;s. Permet de constater l''agissement du contenu sur les diff&eacute;rents supports.\n\n', 'prototype.svg', 'index.php');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
